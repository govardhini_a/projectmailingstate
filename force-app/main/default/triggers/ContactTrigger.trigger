trigger ContactTrigger on Contact (after insert, after update, after delete) {

    if(Trigger.isAfter && Trigger.isInsert){
        ContactHandler.onAfterInsert(Trigger.new);        
    }
    else if(Trigger.isAfter && Trigger.isUpdate){
        ContactHandler.onAfterUpdate(Trigger.oldMap, Trigger.newMap);        
    }
    else if (Trigger.isAfter && Trigger.isDelete){
        ContactHandler.onAfterDelete(Trigger.old);
    }
}