public class CountMailingState implements Database.Batchable<sObject> {
   
    public Database.QueryLocator start(Database.BatchableContext dbc) {
        String stringforQuery = 'SELECT Id, Mailing_State__c FROM Account WHERE Mailing_State__c != NULL';
        return Database.getQueryLocator(stringforQuery);
    }

    public void execute(Database.BatchableContext dbc, List<Account> lstAccounts){

        Map<String, Integer> mapCountMailingState = new Map<String, Integer>();

        for(Account objAccount : lstAccounts){  // itrating over the list of accounts Queryied in start method
            if(!mapCountMailingState.containsKey(objAccount.Mailing_State__c)) { // checking if the mailing state is present in map or not
                mapCountMailingState.put(objAccount.Mailing_State__c, 1); //not present create new
            }
            else {
                mapCountMailingState.put(objAccount.Mailing_State__c, mapCountMailingState.get(objAccount.Mailing_State__c)+1); // present add one to existing
            }
        }

        List<Mailing_State__c> lstMailingStates = [SELECT Id, Name, Number_Of_Accounts__c FROM Mailing_State__c];
        List<Mailing_State__c> lstOfMailingState_TobeUpsert = new List<Mailing_State__c>();
        Set<String> setofExistingMailingState = new Set<String>();
        for(Mailing_State__c objMailingState : lstMailingStates){  // updating the mailing state 
            if(objMailingState.Number_Of_Accounts__c != mapCountMailingState.get(objMailingState.Name)){
                objMailingState.Number_Of_Accounts__c = mapCountMailingState.get(objMailingState.Name);
                lstOfMailingState_TobeUpsert.add(objMailingState);
            }
            setofExistingMailingState.add(objMailingState.Name); // creating a set of mailing state name. 
        }

        for(String mailState : mapCountMailingState.keySet()){
            if(!setofExistingMailingState.contains(mailState)){ // comparing with set if the set does not have the name we are inserting it.
                Mailing_State__c objMailingState = new Mailing_State__c();
                objMailingState.Name = mailState;
                objMailingState.Number_Of_Accounts__c = mapCountMailingState.get(mailState);
                lstOfMailingState_TobeUpsert.add(objMailingState);
            }
        }
        if(!lstOfMailingState_TobeUpsert.isEmpty())
        upsert lstOfMailingState_TobeUpsert;

        List<Mailing_State__c> lstMailingStates_ToDelete = new List<Mailing_State__c>();

        for(Mailing_State__c objMailingState : lstMailingStates){ // checking if the mailing state has any null or empty value. and deleting it.
            if((objMailingState.Number_Of_Accounts__c == NULL) || (objMailingState.Number_Of_Accounts__c == 0)){
                lstMailingStates_ToDelete.add(objMailingState);
            }
        }
        if(!lstMailingStates_ToDelete.isEmpty()){
            delete lstMailingStates_ToDelete;
        }
    }

    public void finish(Database.BatchableContext dbc){
        System.debug('Completed');
    }
}
