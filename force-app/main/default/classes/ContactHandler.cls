public class ContactHandler{
 
    public static void onAfterInsert(List<Contact> lstNewContacts){
        ContactHandler objContactHandler = new ContactHandler();
        objContactHandler.insertContact(lstNewContacts);  
    }
    
    public static void onAfterUpdate(map<Id, Contact> mapContactOld, map<Id, Contact> mapContactNew){
        ContactHandler objContactHandler = new ContactHandler();
        objContactHandler.updateContact(mapContactOld, mapContactNew);  
    }
    
    public static void onAfterDelete(List<Contact> lstOldContact){
        ContactHandler objContactHandler = new ContactHandler();
        objContactHandler.deleteContact(lstOldContact);  
    }
    
    public void insertContact(List<Contact> lstNewContacts){
        Map<Id, String> mapContactLNameAccountId = new Map<Id, String>();
        Set<Id> setContactId = new Set<Id> ();
        for(Contact objContact : lstNewContacts){
            if((objContact.Primary__c == TRUE) && objContact.AccountId != NULL){
                mapContactLNameAccountId.put(objContact.AccountId, objContact.LastName); // getting AccountId and Contact name As a Map. 
                setContactId.add(objContact.Id); // getting only Contact Id.
            }
        }
        List<Account> lstAccountUpdate = new List<Account>();
        for(Id objAccountId : mapContactLNameAccountId.keySet()){ // Updating the Account object with the primary_Contact__c with Contact LastName.
            Account objAccount = new Account(); 
            objAccount.Id = objAccountId;
            objAccount.Primary_Contact__c = mapContactLNameAccountId.get(objAccountId);
            lstAccountUpdate.add(objAccount);        
        }
        if(!lstAccountUpdate.isEmpty())
        update lstAccountUpdate;
        if(mapContactLNameAccountId.size()>0){
        List<Contact> lstContact = [SELECT Id, Primary__c FROM Contact WHERE AccountId IN :mapContactLNameAccountId.keySet() AND Id NOT IN :setContactId];
        // getting only the Records of Related Contact other than the One inserted.
        List<Contact> lstContactUpdate = new List<Contact>();
        for(Contact objContact : lstContact){ // updating the Contact object's Primary field.
            objContact.Primary__c = FALSE;
            lstContactUpdate.add(objContact);
        }
    
        if(!lstContactUpdate.isEmpty())
        update lstContactUpdate; 
        }
    }
    public void updateContact(map<Id, Contact> mapContactOld, map<Id, Contact> mapContactNew){
        Map<Id, String> mapContactLNameAccountId = new Map<Id, String>();
        for(Id ContactId : mapContactNew.keySet()){ // Comparing the Old and new getting whether primary field got updated or not.
            if((mapContactOld.get(ContactId).Primary__c != mapContactNew.get(ContactId).Primary__c) && (mapContactNew.get(ContactId).Primary__c == TRUE) && (mapContactNew.get(ContactId).AccountId != NULL)){
                mapContactLNameAccountId.put(mapContactNew.get(ContactId).AccountId, mapContactNew.get(ContactId).LastName); // getting only the AccountId and ContactLastName in a Map.
            }
        }
        List<Account> lstAccountToUpdate = new List<Account>();
        for(Id AccountId : mapContactLNameAccountId.keySet()){ // updating the Account object with the Primary_Contact__c field.
            Account objAccount = new Account();
            objAccount.id = AccountId;
            objAccount.Primary_Contact__c = mapContactLNameAccountId.get(AccountId);
            lstAccountToUpdate.add(objAccount);
        }
        if(!lstAccountToUpdate.isEmpty())
        update lstAccountToUpdate;
        if(mapContactLNameAccountId.size()>0){
            List<Contact> lstContact=[SELECT Id, Primary__c FROM Contact WHERE AccountId IN :mapContactLNameAccountId.keySet() AND Id NOT IN :mapContactNew.keySet()];
            // getting only the Records of Related Contact other than the One Updated.
            List<Contact> lstContactToUpdate = new List<Contact>();
            for(Contact objContact : lstContact){ // updating the Contact's Primary field. 
                objContact.Primary__c = FALSE;
                lstContactToUpdate.add(objContact);
            }
            if(!lstContactToUpdate.isEmpty())
            update lstContactToUpdate; 
        }
    }
    public void deleteContact(List<Contact> lstOldContact){
        List<Account> lstAccount = new List<Account>();
        for(Contact objContact : lstOldContact){
            if(objContact.Primary__c == TRUE && objContact.AccountId != NULL){ // updating the Account of Primary_Contact__c.
               Account objAccount = new Account();
               objAccount.Id = objContact.AccountId;
               objAccount.Primary_Contact__c = NULL;
               lstAccount.add(objAccount);
           }
        }
        if(!lstAccount.isEmpty())
        update lstAccount;
    }    
}